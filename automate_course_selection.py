import pynput
from pynput.mouse import Listener
from pynput.keyboard import Key
import time
import re

mouse = pynput.mouse.Controller()
keyboard = pynput.keyboard.Controller()

dirty_links = """Python: https://lnkd.in/epdtT8Y
Ionic 3: https://lnkd.in/e8mdY4V
HTML5: https://lnkd.in/eRZVUSN
Android: https://lnkd.in/eHg7kbz
C++: https://lnkd.in/eZvb4rV
Python: https://lnkd.in/ekavMui
C e C++: https://lnkd.in/eKUJ97p
Unity 5: https://lnkd.in/e-ftZCJ
Unreal Engine 4: https://lnkd.in/ekzWpBg
Web com Github pages: https://lnkd.in/eNfr_eS
Git: https://lnkd.in/er4im7H
Wordpress: https://lnkd.in/eYExqay
Jekyll: https://lnkd.in/e4m5Q7q
Wordpress: https://lnkd.in/eMffdp2
React.js: https://lnkd.in/eE67yas
Unity3D: https://lnkd.in/ecM63B6
HTML: https://lnkd.in/eAQFGqy
C++: https://lnkd.in/evqDkeM
Algoritmos e lógica: https://lnkd.in/ehuYnvN
JavaScript: https://lnkd.in/e9EppKs
Qlikview: https://lnkd.in/e5ZufxV
Algoritmos e lógica: https://lnkd.in/e_E3Dry
Python: https://lnkd.in/ejTKa3W
Introdução à programação de computadores: https://goo.gl/Mi3hbF
Terminal Linux: https://goo.gl/vyHAhT
Introdução ao Sistema Operacional Linux: https://goo.gl/iTY5Zj
Introdução a banco de dados com MySQL & PHPMyAdmin: https://goo.gl/J19GG9
Data Science: Visualização de Dados com Python: https://goo.gl/kyy2AD
Introdução ao jQuery: https://goo.gl/3vMv7N
Introdução à linguagem JavaScript: https://goo.gl/1H2Nhn
Introdução à linguagem CSS: https://goo.gl/hdgvRG
Introdução ao PHP orientado a objetos: https://goo.gl/uFSf9b
Curso de programação com Perl: https://goo.gl/f7o1Si
Introdução à linguagem Python: https://goo.gl/oTDWdX
Boas práticas em PHP: https://goo.gl/u2pAfp
BLAST: Ferramenta de Alinhamentos Locais de Sequências: https://goo.gl/TrVrW5
Modelagem de proteínas por homologia: https://goo.gl/mz8uim
Introdução ao Framework Bootstrap: https://goo.gl/8WmsVE
Introdução à Criação de Sites Dinâmicos com PHP: https://goo.gl/qo2Xpw"""

def on_click(x, y, button, pressed):
    browser_bar = (x, y)
    course_subscription = (x, y)
    links = parse(dirty_links)
    subscribe(links, browser_bar, course_subscription)


def parse(links):
    parsed_links = links.split("\n")
    clean_links = []
    for link in parsed_links:
        clean_links.append(re.sub(r'.*: ', '', link))
    return clean_links


def subscribe(links, browser_bar, course_subscription):
    mouse.position = browser_bar
    mouse.press(pynput.mouse.Button.left)
    mouse.release(pynput.mouse.Button.left)
    mouse.press(pynput.mouse.Button.left)
    mouse.release(pynput.mouse.Button.left)
    print(browser_bar, course_subscription)

    for link in links:
        for char in link:
            keyboard.press(char)
            time.sleep(0.05)
            keyboard.release(char)
            time.sleep(0.05)
        keyboard.press(Key.enter)
        time.sleep(0.05)
        keyboard.release(Key.enter)
        time.sleep(0.05)

        # mouse.position = course_subscription
        # mouse.press(pynput.mouse.Button.left)
        # mouse.release(pynput.mouse.Button.left)




with Listener(
        on_click=on_click) as listener:
    listener.join()
